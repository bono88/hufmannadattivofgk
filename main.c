#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>

#define MAX_STRING_LEN 100 //definisco la lunghezza massima per la stringa in input

struct tree_el {
    int value,order;
    char character;
    int isRightSon; //serve per sapere se il nodo corrente è il figlio destro o sinistro rispetto al padre
    struct codify_el * first_cod;
    struct tree_el * right, * left, * father;
    };

typedef struct tree_el node;

typedef struct codify_el{
    int cod;
    struct codify_el *next;
} codify;


char read[MAX_STRING_LEN], sequence[MAX_STRING_LEN];
int insertIndex = 0, orderCode= 1, isNewFind=0;
node *root, *ZERO;

int alreadyRead(char c){
    for(int i=0; read[i] !=0 ;i++)
        if(read[i]==c)
            return 1;
    return 0;
}

void insertAlreadyRead(char c){
    read[insertIndex] = c;
    insertIndex++;
}

void printNodeCode(node *ToPrint){ //metodo per stampare la dofifica di un nodo
    codify *firstCod = ToPrint->first_cod;

    if(firstCod != NULL)
    while(firstCod->next != NULL) {
        firstCod = firstCod->next;
        printf("%d",firstCod->cod);
    }
}

node * createTreeNode(char c){
    //aggiungo il nuovo nodo all'albero (FUNZIONE GIÀ TESTATA E FUNZIONANTE)
    node *newNode = (node *)malloc(sizeof(node));
    newNode->left = ZERO;
    newNode->right = (node *)malloc(sizeof(node));
    newNode->order = orderCode++;


    newNode->right->character = c;
    newNode->right->value= 1;
    newNode->right->isRightSon= 1;
    newNode->right->father = newNode;
    newNode->right->order =orderCode++;
    newNode->right->right =  newNode->right->left = NULL;

    newNode->value = 0;
    newNode->character=NULL;
    newNode->father=ZERO->father;

    //ovviamente se zero non ha un padre non posso dare un padre al nuovo nodo.
    if(ZERO->father!=NULL) {
        newNode->father->left = newNode;
        newNode->isRightSon = 0;
    }

    //questo lo faccio perchè quando lo zero avra come padre "NULL"
    //vuol dire che createTreeNode sarà il nodo di ROOT per tutta la vita del processo
    if(ZERO->father==NULL)
        root = newNode;


    ZERO->father=newNode;
    ZERO->order=orderCode;

    return newNode;
};

node * findNodeInTree(node * tree, char toFind) {
    static node *temp;
    //cerco il nodo corrispondente al carattere
    if (tree != NULL)
        if (tree->character == toFind)
            temp = tree;
        else {
            findNodeInTree(tree->left, toFind);
            findNodeInTree(tree->right, toFind);
        }
    else
        return temp;
}

node * findCodifyInTree(node * tree, char cod[], int cod_length){
//cerco il nodo con la stessa codifica
    static node *temp;
    if(tree == root)
        temp = NULL; //setto la variabile temp a null quando inizio a fare la ricerca
    //cerco il nodo corrispondente al carattere
    if (tree != NULL) {

        codify *firstCod = tree->first_cod->next;
        int check = 1,counter = 0;
        if(firstCod != NULL && (tree == ZERO || tree->character!=NULL)) {
            do {
                if (firstCod->cod != (int)cod[counter] - '0') {
                    //se il carattere della codifica del nodo non corrisponde a
                    // quella del'array cod allora non è il nodo che cerco
                    //ma se il ciclo non entra mai in questa condizione allora vuol dire che ho il nodo giusto!
                    check = 0;
                    break;
                }
                counter++;
                firstCod = firstCod->next;
            }while(firstCod != NULL);
        }
        else{
            //se attualmente il nodo non ha codifica allora non è quello che cerco
            check=0;
        }

        if (check && cod_length == counter) { //mi assicuro che le lunghezze delle due codifiche coincidano
            temp = tree;
        }
        else {
            findCodifyInTree(tree->left, cod, cod_length);
            findCodifyInTree(tree->right, cod, cod_length);
        }
    }
    else
        return temp;
}

node *findNodeSmallerOrderSameValue(node *tree, int value, int order) {
    static node *temp;
    if(isNewFind){
        temp = NULL;
        isNewFind = 0; //controllo per svuotare la variabile temp ogni nuova ricerca
    }
    //cerco il nodo con il valore uguale e l'indice di ordine superiore
    if (tree != NULL) {
        if (tree->value == value && tree->order < order && tree!=root){
            if(temp!=NULL) {
                if (tree->order < temp->order)
                    temp = tree;
            }
            else
                temp = tree;
        }


        findNodeSmallerOrderSameValue(tree->left, value, order);
        findNodeSmallerOrderSameValue(tree->right, value, order);
    }
        return temp;

}

void cleanCodify(node *tree){
    //ciclo tutto l'albero per rimuovere la codifica

    if (tree != NULL){

        tree->first_cod = NULL;

        cleanCodify(tree->left);
        cleanCodify(tree->right);
    }
}

void addCod(node *tree,int codToAdd){
    if(tree->father == NULL){
        //codifica della root univoca... SEMPRE
        tree->first_cod = (codify *) malloc(sizeof(codify));
        tree->first_cod->cod = 0;
        return;
    }

    codify *fatherCod = tree->father->first_cod;


    tree->first_cod = (codify *) malloc(sizeof(codify));
    tree->first_cod->cod = fatherCod->cod;
    fatherCod = fatherCod->next;

    codify *myCod = tree->first_cod;

    while (fatherCod!=NULL){
        myCod->next = (codify *) malloc(sizeof(codify));
        myCod->next->cod = fatherCod->cod;

        fatherCod = fatherCod->next;
        myCod =myCod->next;
    }

    myCod->next = (codify *) malloc(sizeof(codify));
    myCod->next->cod = codToAdd;
}

void updateCodify(node *tree, int codeToAdd){
    //ciclo tutto l'albero per aggiornare la codifica

    if (tree != NULL){

        addCod(tree,codeToAdd);

        updateCodify(tree->left, 0);
        updateCodify(tree->right, 1);
    }
}

void updateTree(node * currentNode){
    while(currentNode->father !=NULL){
        isNewFind = 1; //questa variabile globale identifica l'inizio di una nuova ricerca nell'albero
        node *find = findNodeSmallerOrderSameValue(root, currentNode->value, currentNode->order);
        if(find!=NULL){
            //se ho trovato un nodo procedo con la sostituzione
            int currentIsRight = currentNode->isRightSon;
            int currentOrder = currentNode->order;
            node *currentFather = currentNode->father;

            //Cambio la referenza PADRE->FIGLIO
            if(currentNode->isRightSon)
                currentNode->father->right = find;
            else
                currentNode->father->left = find;

            if(find->isRightSon)
                find->father->right = currentNode;
            else
                find->father->left = currentNode;

            //Cambio la referenza FIGLIO->PADRE
            currentNode->father = find->father;
            currentNode->isRightSon = find->isRightSon;
            currentNode->order = find->order;

            find->father = currentFather;
            find->isRightSon = currentIsRight;
            find->order = currentOrder;

        }
        currentNode->value++;
        currentNode = currentNode->father;
    }
    currentNode->value++;

    //resetto le codifiche
    cleanCodify(root);

    //faccio l'update della codifica
    updateCodify(root,0);
}

void compression(){
    //variabile d'appoggio per aggiornamento albero
    node *currentNode=(node *)malloc(sizeof(node));

    //creo nodo zero
    ZERO = (node *)malloc(sizeof(node));
    ZERO->left = ZERO->right = ZERO->father = NULL;
    ZERO->value = NULL;
    ZERO->character=NULL;

    //inizialmente uso ZERO come root (perchè è l'unico nodo presente)
    root = ZERO;

    //leggo i simboli
    printf("Inserire la stringa da comprimere:\t");
    scanf("%[^\n]s",sequence);

    printf("Stringa compressa: ");

    //faccio passare i simboli
    for(int i=0; sequence[i]!=0;i++){
        //controllo se il simbolo è stato letto
        if(!alreadyRead(sequence[i])){

            //se non è gia stato letto lo aggiungo negli array dei simboli gia letti
            insertAlreadyRead(sequence[i]);

            //inserisco il nuovo nodo all'albero
            currentNode = createTreeNode(sequence[i]);

            //Stampo il codice di zero
            printNodeCode(ZERO);

            //stampo il carattere appena letto
            printf("%c",sequence[i]);

        }
        else {
            //cerco il nodo corrispondente in tutto l'albero
            currentNode=findNodeInTree(root,sequence[i]);
            printNodeCode(currentNode);
        }

        //faccio l'update dell'albero
        updateTree(currentNode);
    }

    printf("\nFINE");
}

void deCompression(){
    //creo nodo zero
    ZERO = (node *)malloc(sizeof(node));
    ZERO->left = ZERO->right = ZERO->father = NULL;
    ZERO->value = NULL;
    ZERO->character=NULL;

    root = ZERO;

    node *currentNode=(node *)malloc(sizeof(node));

    //leggo i simboli
    printf("Inserire la stringa da decomprimere:\t");
    scanf("%[^\n]s",sequence);
    printf("Stringa decompressa: ");

    //creo le "fondamenta dell'albero con il primo carattere presente nella stringa
    //ovviamente non cerco la codifica di zero perchè l'albero effettivamente non esiste ancora."
    updateTree(createTreeNode(sequence[0]));
    printf("%c",sequence[0]);
    //instanzio un buffer di lettura. Perchè mi serve "ricordare" i caratteri precedenti nel caso stia leggendo una codifica.
    char buffer[10];
    int bufferLenght=0;


    //variabile di controllo che identifica l'arrivo di un nuovo carttere
    int nextIsNew = 0;

    //faccio passare i simboli (inizio da uno perchè il primo carattere l'ho già processato)
    for(int i=1; sequence[i]!=0;i++){
        //se ho una codifica devo verificare se è quella del nodo zero.
        //Se è così allora il carattere successivo è un nuovo carattere
        //altrimenti sarà la codifica di un carattere esistente.
        //In tal caso dovrò aggironare il "conteggio nell'albero e ripesarlo"

        if(!nextIsNew){
            buffer[bufferLenght++] = sequence[i];
            node *findNode = findCodifyInTree(root,buffer,bufferLenght);
            if(findNode!=NULL){
                if(findNode == ZERO){
                    //il prossimo carattere sarà un nuovo carattere
                    nextIsNew = 1;
                }
                else{
                    //Vado a ripesare l'albero partendo da quel nodo perchè è già presente nell'albero
                    updateTree(findNode);
                    printf("%c",findNode->character);
                    bufferLenght = 0;
                }
            }

        }
        else{
            //inserisco il nuovo carattere nell'albero
            updateTree(createTreeNode(sequence[i]));
            printf("%c",sequence[i]);
            nextIsNew = 0;
            bufferLenght = 0;
        }


    }
}

int main()  {
    printf("Menu:\n");
    printf("\t1. Compressione\n");
    printf("\t2. Decompressione\n");
    char menuSelect = (char)getchar();
    getchar();
    switch (menuSelect) {
        case '1':
            compression();
            break;
        case '2':
            deCompression();
            break;
        default:
            printf("Selezione non valida!");
    }

    return 0;
}
