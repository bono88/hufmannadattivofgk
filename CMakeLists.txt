cmake_minimum_required(VERSION 3.2)
project(HufmannAdattivoFGK)

set(CMAKE_C_STANDARD 11)

set(SOURCE_FILES main.c)
add_executable(HufmannAdattivoFGK ${SOURCE_FILES})